package com.cdac.model;

public class LoginPageModel {

	private String email;
	private String password;

	public LoginPageModel() {

	}

	public LoginPageModel(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "LoginPage [email=" + email + ", password=" + password + "]";
	}

}
