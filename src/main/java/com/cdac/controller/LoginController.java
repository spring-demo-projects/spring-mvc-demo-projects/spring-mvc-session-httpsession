package com.cdac.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.cdac.model.LoginPageModel;
import com.cdac.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@GetMapping(value = "login", produces = "text/html")
	public String showLoginPage(Model model) {

		model.addAttribute("command", new LoginPageModel());
		return "loginPage";
	}

	@PostMapping(value = "login", produces = "text/html")
	public String login(@ModelAttribute LoginPageModel loginPageModel, HttpSession httpSession) {
		if (loginPageModel.getEmail().equals(loginPageModel.getPassword())) {
			httpSession.setAttribute("name", loginService.getName());
			httpSession.setAttribute("email", loginPageModel.getEmail());
			httpSession.setAttribute("addharNumber", loginService.getAddharNumber());
			httpSession.setAttribute("mobileNumber", loginService.getMobileNumber());
			httpSession.setAttribute("panNumber", loginService.getPanNumber());
			return "welcome";
		} else
			return "loginFailed";
	}

	@GetMapping(value = "dashboard", produces = "text/html")
	public String showDashboard(Model model, HttpSession httpSession) {
		model.addAttribute("email", httpSession.getAttribute("email"));
		return "dashboard";

	}

	@GetMapping(value = "logout", produces = "text/html")
	public String logout(HttpSession httpSession) {
		httpSession.invalidate();
		return "logoutPage";
	}

}
